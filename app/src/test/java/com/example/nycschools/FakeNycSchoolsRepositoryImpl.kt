package com.example.nycschools

import com.example.nycschools.data.HighSchoolProfile
import com.example.nycschools.data.NYCSchoolsRepository
import com.example.nycschools.data.ScoreProfile

class FakeNycSchoolsRepositoryImpl : NYCSchoolsRepository {
    override suspend fun getHighSchoolProfiles(): List<HighSchoolProfile> {
        return listOf(
            HighSchoolProfile("A", "1", "desc1",
            "n1", "", "", "", "", "",
            "", "", "", "", "", "", "",
            "", ""),
            HighSchoolProfile("B", "2", "desc2",
                "n1", "", "", "", "", "",
                "", "", "", "", "", "", "",
                "", "")
        )
    }

    override suspend fun getSatScoreProfile(dbn: String): ScoreProfile? {
        return ScoreProfile(1,
            100, 100, 100)
    }
}