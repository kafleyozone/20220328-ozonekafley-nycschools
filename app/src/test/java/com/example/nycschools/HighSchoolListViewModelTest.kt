package com.example.nycschools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nycschools.data.Resource
import com.example.nycschools.ui.viewmodels.HighSchoolListViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class HighSchoolListViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    @ExperimentalCoroutinesApi
    var coroutinesTestRule = CoroutinesTestRule()

    @Test
    fun init_exposesSuccessResource() {
        // create the viewModel under test
        val highSchoolListViewModel = HighSchoolListViewModel(FakeNycSchoolsRepositoryImpl())

        // fetch mock data
        highSchoolListViewModel.fetchSchoolProfiles()

        // live data now exposing Success resource with expected data
        highSchoolListViewModel.schoolProfiles.observeForever {
            assert(it is Resource.Success)
            assert(it.data != null && it.data!!.size == 2)
            assert(it.data!!.first().dbn == "A")
        }
    }
}