package com.example.nycschools.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.nycschools.R
import com.example.nycschools.data.HighSchoolProfile
import com.example.nycschools.data.Resource
import com.example.nycschools.databinding.HighSchoolListFragmentBinding
import com.example.nycschools.ui.*
import com.example.nycschools.ui.viewmodels.HighSchoolListViewModel
import com.google.android.material.transition.MaterialFadeThrough
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HighSchoolListFragment : Fragment(), SchoolListItemListener {

    // Caution: this binding is only valid from onCreateView to onDestroy!
    private var _binding: HighSchoolListFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: HighSchoolListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        exitTransition = MaterialFadeThrough()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = HighSchoolListFragmentBinding.inflate(inflater)

        val adapter = SchoolListAdapter(this, requireContext())
        binding.schoolsRecyclerView.adapter = adapter

        // Observes the state of the school profiles live data and updates the UI accordingly
        viewModel.schoolProfiles.observe(viewLifecycleOwner) { res ->
            when (res) {
                is Resource.Loading -> {
                    toggleLoadingVisibility(true)
                }
                is Resource.Error -> {
                    toggleLoadingVisibility(false)
                    showSnackbar(requireView(), R.string.network_error_message)
                }
                is Resource.Success -> {
                    toggleLoadingVisibility(false)
                    res.data?.let {
                        adapter.submitList(it)
                    } ?: showSnackbar(requireView(), R.string.network_error_message)
                }
            }
        }

        return binding.root
    }

    /*
    * Centralized place to toggle views during loading/non-loading states
    * */
    private fun toggleLoadingVisibility(isLoading: Boolean) {
        val visibility = if (isLoading) View.VISIBLE else View.GONE
        binding.loadingMessageTextView.visibility = visibility
        binding.progressIndicator.visibility = visibility
    }

    /*
    * Called by the list item view holder when user taps on the list item's root layout
    * */
    override fun handleOnSchoolClick(schoolProfile: HighSchoolProfile) {
        findNavController().navigate(HighSchoolListFragmentDirections
            .actionHighSchoolListToHighSchoolDetails(schoolProfile))
    }

    /*
    * Called by the list item school name on click
    * */
    override fun handleOnLinkClick(urlString: String) {
        try {
            val intent: Intent? = createImplicitIntent(ActionType.WEB, urlString)
            if (intent != null) startActivity(intent)
        } catch (e: Exception) {
            // this occurs if the url is malformed or there is no application on the device
                // that can handle this intent
            Log.e(javaClass.simpleName, "Couldn't go to the website", e)
            showSnackbar(requireView(), R.string.missing_website_error)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        // recommended by Android docs to avoid accidentally leaking views
        _binding = null
    }
}