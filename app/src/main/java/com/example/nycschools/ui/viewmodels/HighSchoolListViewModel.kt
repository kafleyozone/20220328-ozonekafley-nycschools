package com.example.nycschools.ui.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.data.HighSchoolProfile
import com.example.nycschools.data.NYCSchoolsRepository
import com.example.nycschools.data.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HighSchoolListViewModel @Inject constructor(
    private val repository: NYCSchoolsRepository
) : ViewModel() {

    private val _schoolProfiles =
        MutableLiveData<Resource<List<HighSchoolProfile>>>(Resource.Loading())
    val schoolProfiles: LiveData<Resource<List<HighSchoolProfile>>> get() = _schoolProfiles

    init {
        // as soon as the viewModel is created and enters the Start/Resume lifecycle state,
        // begin fetching the school profiles on the IO thread
        fetchSchoolProfiles()
    }

    fun fetchSchoolProfiles() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _schoolProfiles.postValue(Resource.Success(repository.getHighSchoolProfiles()))
            } catch (e: Exception) {
                Log.e(javaClass.simpleName, "Error getting school profiles", e)
                _schoolProfiles.postValue(Resource.Error())
            }
        }
    }
}