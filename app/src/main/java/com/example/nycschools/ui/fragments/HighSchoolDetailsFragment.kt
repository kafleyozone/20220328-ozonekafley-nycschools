package com.example.nycschools.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.nycschools.R
import com.example.nycschools.data.HighSchoolProfile
import com.example.nycschools.data.Resource
import com.example.nycschools.databinding.HighSchoolDetailsFragmentBinding
import com.example.nycschools.ui.*
import com.example.nycschools.ui.viewmodels.HighSchoolDetailsViewModel
import com.google.android.material.transition.MaterialFadeThrough
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HighSchoolDetailsFragment : Fragment() {

    // Caution: this binding is only valid from onCreateView to onDestroy!
    private var _binding: HighSchoolDetailsFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: HighSchoolDetailsViewModel by viewModels()
    private val args by navArgs<HighSchoolDetailsFragmentArgs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enterTransition = MaterialFadeThrough()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = HighSchoolDetailsFragmentBinding.inflate(inflater)

        // list screen passes in the entire school profile object associated with the list item
        args.schoolProfile.let { school ->
            with(binding) {
                nameTextView.text = school.name
                overviewTextView.text = school.description
                loadStaticMapImageIntoImageView(school, mapImageView)
                setupTopRowButtons(school)
            }
            viewModel.start(school.dbn)
        }

        // observe the SAT profile live data and update the UI accordingly
        viewModel.scoreProfile.observe(viewLifecycleOwner) { res ->
            when (res) {
                is Resource.Loading -> {
                    binding.totalNumTestTakersTextView.text = getString(R.string.loading_message)
                    binding.scoresLoader.visibility = View.VISIBLE
                }
                is Resource.Error -> {
                    binding.totalNumTestTakersTextView.visibility = View.GONE
                    binding.scoresLoader.visibility = View.GONE
                    showSnackbar(requireView(), R.string.network_error_message)
                }
                is Resource.Success -> {
                    res.data?.let {
                        // if there are any invalid (negative) scores -- edge case -- let the scores
                        // views reflect that there is no valid data for this school's scores
                        if (it.numTestTakers < 0 || it.mathAverageScore < 0
                            || it.readingAverageScore < 0 || it.writingAverageScore < 0) {
                            handleNoScoresData()
                            return@let
                        }
                        with(binding) {
                            totalNumTestTakersTextView.text =
                                getString(R.string.with_test_takers_label, it.numTestTakers)
                            scoresLoader.visibility = View.GONE
                            mathScoreChip.text = it.mathAverageScore.toString()
                            writingScoreChip.text = it.writingAverageScore.toString()
                            readingScoreChip.text = it.readingAverageScore.toString()
                        }
                    } ?: handleNoScoresData()
                }
            }
        }

        return binding.root
    }

    /*
     * Let the score views reflect if there is no valid data for this school's scores
     */
    private fun handleNoScoresData() {
        with(binding) {
            scoresLoader.visibility = View.GONE
            totalNumTestTakersTextView.text = getString(R.string.no_data_message)
        }
    }

    /*
    * Helper methods to setup the four round buttons to help the user contact and locate the school
    * */
    private fun setupTopRowButtons(school: HighSchoolProfile) {
        with(binding) {
            websiteButton.setOnClickListener {
                handleOnContactButtonClick(school.website, ActionType.WEB)
            }
            callButton.setOnClickListener {
                handleOnContactButtonClick(school.phoneNumber, ActionType.CALL)
            }
            emailButton.setOnClickListener {
                handleOnContactButtonClick(school.email, ActionType.EMAIL)
            }
            locationButton.setOnClickListener {
                // given more time, I would figure out how to handle cases where Google Maps is not
                // present on the device and let Google Maps open the location
                // with an implicit intent otherwise
                requireContext().copyString(school.streetAddress, school.city,
                    school.state, school.zip)
                showSnackbar(requireView(), R.string.copied_message)
            }
        }
    }

    /*
    * Handle any of 4 actions from the Contact/Locate row on the Details page.
    * @param authority is the url/email/phone/coordinates
    * */
    private fun handleOnContactButtonClick(authority: String, action: ActionType) {
        try {
            val intent = createImplicitIntent(action, authority)
            if (intent != null) startActivity(intent)
        } catch (e: Exception) {
            // this occurs if the url is malformed or there is nothing on the device
            // that can handle this intent
            Log.e(javaClass.simpleName, "Couldn't handle", e)
            showSnackbar(requireView(), R.string.missing_website_error)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        // recommended by Android docs to avoid accidentally leaking views
        _binding = null
    }
}