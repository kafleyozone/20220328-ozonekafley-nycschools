package com.example.nycschools.ui.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.data.NYCSchoolsRepository
import com.example.nycschools.data.Resource
import com.example.nycschools.data.ScoreProfile
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HighSchoolDetailsViewModel @Inject constructor(
    private val repository: NYCSchoolsRepository
) : ViewModel() {

    private val _scoreProfile =
        MutableLiveData<Resource<ScoreProfile?>>(Resource.Loading())
    val scoreProfile: LiveData<Resource<ScoreProfile?>> get() = _scoreProfile

    // cannot start in init because we need the dbn of the school passed in. If more time, would
    // explore use of ViewModelFactory to provide this value
    // initiate fetching of school's score profile on the IO thread
    fun start(dbn: String) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _scoreProfile.postValue(Resource.Success(repository.getSatScoreProfile(dbn)))
            } catch (e: Exception) {
                Log.e(javaClass.simpleName, "Error getting score profiles for $dbn", e)
                _scoreProfile.postValue(Resource.Error())
            }
        }
    }
}