package com.example.nycschools.ui

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.annotation.StringRes
import com.example.nycschools.BuildConfig
import com.example.nycschools.data.HighSchoolProfile
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso

const val NYC_TOKEN_HEADER = "X-App-Token"


 /*
 * These are the four actions that can be taken to direct the user to an external app, if available.
 * */
enum class ActionType {
    WEB, CALL, EMAIL
}

/*
* Based on the above actions, we create the appropriate intent.
* */
fun createImplicitIntent(action: ActionType, authority: String): Intent? {
    var intent: Intent? = null
    if (authority.isEmpty()) return intent
    when (action) {
        ActionType.CALL -> {
            val uri = Uri.parse(
                "tel:" +
                        authority.replace("-", "")
            )
            intent = Intent(Intent.ACTION_DIAL, uri)
        }
        ActionType.EMAIL -> {
            intent = Intent(Intent.ACTION_SENDTO).apply {
                data = Uri.parse("mailto:${authority}") // to invoke only e-mail clients
                putExtra(Intent.EXTRA_EMAIL, arrayOf(authority))
            }
        }
        ActionType.WEB -> {
            val uri = Uri.parse("https://$authority")
            intent = Intent(Intent.ACTION_VIEW, uri)
        }
    }
    return intent
}

/*
* Extension function on the Context object to copy strings (concatenated w/ a space) to the
* system clipboard.
* */
fun Context.copyString(vararg addressPieces: String) {
    val address = addressPieces.joinToString(" ")
    val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    clipboardManager.setPrimaryClip(ClipData.newPlainText("Address", address))
}

fun showSnackbar(view: View, @StringRes messageResId: Int) {
    Snackbar.make(view, messageResId, Snackbar.LENGTH_SHORT)
        .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
        .show()
}

/*
    * Helper method to load a static map image with marker into the given image view with the
    * school profile object
    * With more time, I would use placeholder and error images. Right now, if the static map image
    * is not present, the imageView will simply show a gray background
    * */
fun loadStaticMapImageIntoImageView(profile: HighSchoolProfile, imageView: ImageView) {
    getMapBoxUrl(profile.latitude, profile.longitude)?.let { urlString ->
        Picasso.get()
            .load(urlString)
            .into(imageView)
    }
}

/*
* Produces a URL string for a specific lat,long Mapbox static image fetch.
* With more time, I would parameterize more of the API (marker type, color, map type, etc.)
* */
fun getMapBoxUrl(latitude: String, longitude: String): Uri? {
    if (latitude.isBlank() || longitude.isBlank()) return null
    val url = Uri.Builder()
        .scheme("https")
        .authority("api.mapbox.com")
        .appendPath("styles")
        .appendPath("v1")
        .appendPath("mapbox")
        .appendPath("streets-v11")
        .appendPath("static")
        .appendEncodedPath("pin-l-college+f74e4e($longitude,$latitude)")
        .appendEncodedPath("$longitude,$latitude,14")
        .appendPath("500x400")
        .appendQueryParameter("access_token", BuildConfig.MAPBOX_ACCESS_TOKEN)
        .build()
    Log.i("UIUtils", "static image url: $url")
    return url
}