package com.example.nycschools.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.R
import com.example.nycschools.data.HighSchoolProfile
import com.example.nycschools.databinding.HighSchoolListItemBinding
import com.example.nycschools.ui.SchoolListAdapter.ViewHolder.Companion.from

/*
* A convenience adapter for the main list of schools on the home screen. Using ListAdapter to reduce
* boilerplate and improve performance of the recycler view (diffs items asynchronously)
* */
class SchoolListAdapter(private val listener: SchoolListItemListener, private val context: Context)
    : ListAdapter<HighSchoolProfile, SchoolListAdapter.ViewHolder>(SchoolDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return from(parent, listener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, context)
    }

    class ViewHolder(
        private val binding: HighSchoolListItemBinding,
        private val listener: SchoolListItemListener
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(schoolProfile: HighSchoolProfile, context: Context) {
            with(binding) {
                schoolNameTextView.text = schoolProfile.name
                abridgedDescriptionTextView.text = schoolProfile.description
                locationsTextView.text = context.getString(R.string.school_list_item_location_label,
                    schoolProfile.neighborhood, schoolProfile.city)
                itemLayout.setOnClickListener { listener.handleOnSchoolClick(schoolProfile) }
                if (schoolProfile.website.isNotEmpty()) {
                    linkImage.visibility = View.VISIBLE
                    schoolNameTextView.setOnClickListener {
                        listener.handleOnLinkClick(schoolProfile.website)
                    }
                } else {
                    linkImage.visibility = View.GONE // necessary because views are recycled.
                }
            }
        }

        companion object {
            fun from(parent: ViewGroup, listener: SchoolListItemListener) : ViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = HighSchoolListItemBinding.inflate(inflater, parent, false)
                return ViewHolder(binding, listener)
            }
        }

    }
}

// object to help AsyncListDiff determine when items have been added, removed, and changed
class SchoolDiffCallback : DiffUtil.ItemCallback<HighSchoolProfile>() {
    override fun areItemsTheSame(oldItem: HighSchoolProfile,
                                 newItem: HighSchoolProfile): Boolean {
        return oldItem.dbn == newItem.dbn
    }
    override fun areContentsTheSame(oldItem: HighSchoolProfile,
                                    newItem: HighSchoolProfile): Boolean {
        return oldItem == newItem
    }
}

interface SchoolListItemListener {
    fun handleOnSchoolClick(schoolProfile: HighSchoolProfile)
    fun handleOnLinkClick(urlString: String)
}
