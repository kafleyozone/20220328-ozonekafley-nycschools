package com.example.nycschools.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class HighSchoolProfile(
    val dbn: String,
    val name: String,
    val description: String,
    val neighborhood: String,
    val phoneNumber: String,
    val email: String,
    val website: String,
    val streetAddress: String,
    val city: String,
    val state: String,
    val zip: String,
    val latitude: String,
    val longitude: String,
) : Parcelable

data class ScoreProfile(
    val numTestTakers: Int,
    val readingAverageScore: Int,
    val mathAverageScore: Int,
    val writingAverageScore: Int
)