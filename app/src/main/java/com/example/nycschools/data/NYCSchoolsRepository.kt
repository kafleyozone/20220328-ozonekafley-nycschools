package com.example.nycschools.data

interface NYCSchoolsRepository {
    suspend fun getHighSchoolProfiles(): List<HighSchoolProfile>
    suspend fun getSatScoreProfile(dbn: String): ScoreProfile?
}