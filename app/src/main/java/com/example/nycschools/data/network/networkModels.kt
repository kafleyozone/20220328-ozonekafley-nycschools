package com.example.nycschools.data.network

import com.example.nycschools.data.HighSchoolProfile
import com.example.nycschools.data.ScoreProfile
import com.google.gson.annotations.SerializedName

data class NetworkHighSchoolProfile(
    @SerializedName("dbn") val dbn: String?,
    @SerializedName("school_name") val name: String?,
    @SerializedName("overview_paragraph") val description: String?,
    @SerializedName("neighborhood") val neighborhood: String?,
    @SerializedName("phone_number") val phoneNumber: String?,
    @SerializedName("school_email") val email: String?,
    @SerializedName("website") val website: String?,
    @SerializedName("primary_address_line_1") val streetAddress: String?,
    @SerializedName("city") val city: String?,
    @SerializedName("state_code") val state: String?,
    @SerializedName("zip") val zip: String?,
    @SerializedName("latitude") val latitude: String?,
    @SerializedName("longitude") val longitude: String?,
)

data class NetworkScoreProfile(
    @SerializedName("num_of_sat_test_takers") val numTestTakers: String?,
    @SerializedName("sat_critical_reading_avg_score") val readingAverageScore: String?,
    @SerializedName("sat_math_avg_score") val mathAverageScore: String?,
    @SerializedName("sat_writing_avg_score") val writingAverageScore: String?,
)

fun NetworkHighSchoolProfile.toHighSchoolProfile() = HighSchoolProfile(
    dbn ?: "",
    name?.trim() ?: "N/A",
    description?.trim() ?: "Not provided by school.",
    neighborhood?.trim() ?: "",
    phoneNumber?.trim() ?: "N/A",
    email?.trim() ?: "N/A",
    website?.trim() ?: "N/A",
    streetAddress?.trim() ?: "Not provided.",
    city?.trim() ?: "New York City",
    state?.trim() ?: "NY",
    zip?.trim() ?: "",
    latitude?.trim() ?: "",
    longitude?.trim() ?: ""
)

fun NetworkScoreProfile.toScoreProfile() = ScoreProfile(
    numTestTakers?.toIntOrNull() ?: -1,
    readingAverageScore?.toIntOrNull() ?: -1,
    mathAverageScore?.toIntOrNull() ?: -1,
    writingAverageScore?.toIntOrNull() ?: -1
)