package com.example.nycschools.data

/*
* This class makes it simpler to handle UI state as network calls are made. Inspired by various
* Google Developer guides and blogs. When in the Success state, we can hold data of any type
* that is usually returned by a network request or database operation.
* */

sealed class Resource<T>(val data: T? = null, val message: String? = null) {
    class Success<T>(data: T) : Resource<T>(data)
    class Error<T>(message: String? = null, data: T? = null) : Resource<T>(data, message)
    class Loading<T>: Resource<T>()
}