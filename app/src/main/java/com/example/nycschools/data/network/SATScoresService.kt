package com.example.nycschools.data.network

import com.example.nycschools.BuildConfig
import retrofit2.http.GET
import retrofit2.http.Query

interface SATScoresService {

    @GET(BuildConfig.NYC_SAT_API)
    suspend fun getScoreProfileBySchool(@Query("dbn") dbn: String) : List<NetworkScoreProfile>
}