package com.example.nycschools.data.network

import com.example.nycschools.BuildConfig
import com.example.nycschools.ui.NYC_TOKEN_HEADER
import okhttp3.Interceptor
import okhttp3.Response

class AuthorizationInterceptor : Interceptor {

    /*
    * Add the app token to every request when connecting to the NYC open data API
    * */
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val newRequest = original.newBuilder()
            .addHeader(NYC_TOKEN_HEADER, BuildConfig.NYC_APP_TOKEN)
            .method(original.method, original.body)
            .build()
        return chain.proceed(newRequest)
    }
}