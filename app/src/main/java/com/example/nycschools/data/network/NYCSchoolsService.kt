package com.example.nycschools.data.network

import com.example.nycschools.BuildConfig
import retrofit2.http.GET

interface NYCSchoolsService {

    @GET(BuildConfig.NYC_SCHOOLS_API)
    suspend fun getHighSchoolProfiles() : List<NetworkHighSchoolProfile?>
}