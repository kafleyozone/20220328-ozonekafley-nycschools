package com.example.nycschools.data

import com.example.nycschools.data.network.*
import javax.inject.Inject

class NYCSchoolsRepositoryImpl @Inject constructor(
    private val schoolsService: NYCSchoolsService,
    private val scoresService: SATScoresService
) : NYCSchoolsRepository {

    /*
    * Fetch a rather large json and massage into an appropriate structure. This will happen on the
    * IO thread to avoid blocking the Main thread.
    * */
    override suspend fun getHighSchoolProfiles(): List<HighSchoolProfile> =
        schoolsService.getHighSchoolProfiles()
            .filterNotNull()
            .map { it.toHighSchoolProfile() }

    /*
    * The SAT score profile comes in a list of one element, so we can extract it and return.
    * */
    override suspend fun getSatScoreProfile(dbn: String): ScoreProfile? =
        scoresService.getScoreProfileBySchool(dbn).firstOrNull()?.toScoreProfile()
}