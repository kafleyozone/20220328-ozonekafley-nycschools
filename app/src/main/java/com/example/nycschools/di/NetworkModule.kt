package com.example.nycschools.di

import android.content.Context
import com.example.nycschools.BuildConfig
import com.example.nycschools.data.network.AuthorizationInterceptor
import com.example.nycschools.data.network.NYCSchoolsService
import com.example.nycschools.data.network.SATScoresService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    fun provideHttpLoggingInterceptor() = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    @Provides
    fun provideRetrofitCache(@ApplicationContext context: Context): Cache {
        val cacheSize: Long = 10 * 1024 * 1024 // 10 MB cache
        return Cache(context.cacheDir, cacheSize)
    }

    @Provides
    fun providesOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor, cache: Cache) =
        OkHttpClient.Builder()
            .cache(cache) // enables response caching to make the app offline-ready
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(AuthorizationInterceptor()) // to add app token to NYC open data request
            .build()

    @Provides
    fun provideNycSchoolsRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.NYC_BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Provides
    fun provideNycSchoolsService(retrofit: Retrofit): NYCSchoolsService =
        retrofit.create(NYCSchoolsService::class.java)

    @Provides
    fun provideSatScoresService(retrofit: Retrofit): SATScoresService =
        retrofit.create(SATScoresService::class.java)

}