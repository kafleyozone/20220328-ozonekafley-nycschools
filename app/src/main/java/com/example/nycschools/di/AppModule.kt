package com.example.nycschools.di

import com.example.nycschools.data.NYCSchoolsRepository
import com.example.nycschools.data.NYCSchoolsRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    @Binds
    abstract fun provideSchoolsRepository(impl: NYCSchoolsRepositoryImpl) : NYCSchoolsRepository
}